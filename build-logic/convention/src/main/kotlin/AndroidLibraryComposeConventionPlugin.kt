import andrew.misterio05.convention.configureAndroidCompose
import com.android.build.gradle.LibraryExtension
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

class AndroidLibraryComposeConventionPlugin : AndroidLibraryConventionPlugin() {
    override fun apply(target: Project) {
        super.apply(target)
        target.extensions.configure<LibraryExtension>(target::configureAndroidCompose)
    }
}
