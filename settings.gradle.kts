pluginManagement {
    includeBuild("build-logic")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "VoiceNotes"

fun module(projectPath: String) {
    val moduleName = ":${projectPath.substringAfterLast('/')}"
    include(moduleName)
    project(moduleName).projectDir = File("$rootDir/$projectPath")
}

module("sources/app")
module("sources/features/main/domain_main")
module("sources/features/main/feature_main")
module("sources/features/notes/domain_notes")
module("sources/features/notes/feature_notes")
module("sources/features/record/domain_record")
module("sources/features/record/feature_record")
module("sources/tea")
module("sources/uikit")
module("sources/audiorecord")
