package andrew.misterio05.tea

data class ReducerResult<S : State>(
    val state: S,
    val effects: Set<Effect>,
)
