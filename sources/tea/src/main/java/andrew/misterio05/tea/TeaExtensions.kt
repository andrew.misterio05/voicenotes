package andrew.misterio05.tea

infix fun <S : State> S.with(effects: Set<Effect>) = ReducerResult(this, effects)

fun <S : State> S.just() = ReducerResult(this, emptySet())

infix fun <S : State> S.with(effect: Effect?) = ReducerResult(this, setOfNotNull(effect))

infix fun Effect.andThen(effect: Effect?): Set<Effect> = setOfNotNull(this, effect)

infix fun Set<Effect>.andThen(effect: Effect): Set<Effect> = this + effect

infix fun <S : State> ReducerResult<S>.andThen(other: Effect?) = ReducerResult(state, (effects + other).filterNotNull().toSet())
