package andrew.misterio05.tea

interface EffectHandler<E : Effect> {
    suspend fun execute(
        effect: E,
        dispatch: (Event) -> Unit,
    )
}
