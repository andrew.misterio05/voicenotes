package andrew.misterio05.tea

fun interface Reducer<S : State, E : Event> {
    operator fun invoke(
        state: S,
        event: E,
    ): ReducerResult<S>
}
