package andrew.misterio05.audiorecord

import android.media.MediaRecorder
import android.media.MicrophoneDirection
import android.os.Build
import java.io.File

class AudioRecorder(
    cacheDir: String,
    private val recordCore: MediaRecorder,
) : Recorder {
    override var state: RecorderState = RecorderState.None
    private val cacheFileFile = File("$cacheDir/cache_file.3gp")

    init {
        recordCore.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(cacheFileFile)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                setPreferredMicrophoneDirection(MicrophoneDirection.MIC_DIRECTION_TOWARDS_USER)
            }
        }
    }

    override fun start(path: String) {
        removeCache()
        state = RecorderState.InProgress(path)
        runCatching { recordCore.prepare() }
        recordCore.start()
    }

    override fun cancel() {
        if (state is RecorderState.InProgress) {
            stopRecord()
            removeCache()
            state = RecorderState.None
        }
    }

    override fun save() {
        val currentState = state
        if (currentState is RecorderState.InProgress) {
            stopRecord()
            saveCache(currentState.path)
            state = RecorderState.None
        }
    }

    private fun removeCache() {
        cacheFileFile.delete()
    }

    private fun saveCache(path: String) {
        cacheFileFile.renameTo(File(path))
    }

    private fun stopRecord() {
        recordCore.apply {
            runCatching { stop() }
            release()
        }
    }
}
