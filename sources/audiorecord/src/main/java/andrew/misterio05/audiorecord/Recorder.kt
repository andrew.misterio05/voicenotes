package andrew.misterio05.audiorecord

interface Recorder {
    val state: RecorderState

    fun start(path: String)

    fun cancel()

    fun save()
}
