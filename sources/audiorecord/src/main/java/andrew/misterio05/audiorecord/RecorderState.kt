package andrew.misterio05.audiorecord

sealed interface RecorderState {
    data object None : RecorderState

    data class InProgress(val path: String) : RecorderState
}
