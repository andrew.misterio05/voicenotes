@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    id("android.application")
    kotlin("kapt")
}

android {
    namespace = "andrew.misterio05.voicenotes"

    defaultConfig {
        applicationId = "andrew.misterio05.voicenotes"
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro",
            )
        }
    }
}

dependencies {
    implementation(project(":domain_main"))
    implementation(project(":feature_main"))
    implementation(project(":domain_notes"))
    implementation(project(":feature_notes"))
    implementation(project(":domain_record"))
    implementation(project(":feature_record"))
    implementation(project(":audiorecord"))
    implementation(project(":tea"))
    implementation(libs.dagger)

    implementation(libs.core.ktx)
    implementation(libs.lifecycle.runtime.ktx)
    implementation(libs.activity.compose)
    implementation(libs.bundles.view)

    kapt(libs.dagger.compiler)
}
