package andrew.misterio05.voicenotes.ui

import andrew.misterio05.domain_notes.NotesState
import andrew.misterio05.domain_record.RecordState
import andrew.misterio05.feature_main.MainScreen
import andrew.misterio05.feature_notes.NotesScreen
import andrew.misterio05.feature_record.RecordScreen
import andrew.misterio05.tea.Event
import andrew.misterio05.tea.State
import andrew.misterio05.voicenotes.App
import andrew.misterio05.voicenotes.ui.theme.VoiceNotesTheme
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier

class MainActivity : ComponentActivity() {
    private val app get() = application as App

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            VoiceNotesTheme {
                val state by app.stateHolder.state.collectAsState()
                val dispatch = app.stateHolder::dispatch
                state.ui
                    ?.let { mainState ->
                        MainScreen(
                            modifier = Modifier.fillMaxSize(),
                            state = mainState,
                            screen = { modifier, state ->
                                RenderScreen(
                                    modifier = modifier,
                                    state = state,
                                    dispatch = dispatch,
                                )
                            },
                            bottomPanel = { modifier, state ->
                                RenderScreen(
                                    modifier = modifier,
                                    state = state,
                                    dispatch = dispatch,
                                )
                            },
                            dispatch = dispatch,
                        )
                    }
                    ?: finish()
            }
        }
    }

    @Composable
    private fun RenderScreen(
        modifier: Modifier,
        state: State,
        dispatch: (Event) -> Unit,
    ) {
        when (state) {
            is NotesState -> NotesScreen(modifier = modifier, state = state, dispatch = dispatch)
            is RecordState -> RecordScreen(modifier = modifier, state = state, dispatch = dispatch)
        }
    }
}
