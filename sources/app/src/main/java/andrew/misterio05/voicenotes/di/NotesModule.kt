package andrew.misterio05.voicenotes.di

import andrew.misterio05.domain_notes.NotesRepository
import andrew.misterio05.feature_notes.data.NotesRepositoryMocks
import dagger.Module
import dagger.Provides

@Module
class NotesModule {
    @Provides
    fun notesRepository(): NotesRepository = NotesRepositoryMocks
}
