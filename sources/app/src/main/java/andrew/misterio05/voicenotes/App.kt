package andrew.misterio05.voicenotes

import andrew.misterio05.voicenotes.di.AppComponent
import andrew.misterio05.voicenotes.feature.AppEvent
import andrew.misterio05.voicenotes.feature.AppState
import android.app.Application
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class App : Application() {
    val stateHolder by lazy(LazyThreadSafetyMode.SYNCHRONIZED) { StateHolder(AppState()) }
    val component: AppComponent by lazy(LazyThreadSafetyMode.SYNCHRONIZED, AppComponent::create)

    override fun onCreate() {
        super.onCreate()
        val handler = component.effectHandler
        CoroutineScope(SupervisorJob() + CoroutineExceptionHandler(::exceptionHandler)).launch {
            stateHolder.effectFlow.collect { effect ->
                handler.execute(effect, stateHolder::dispatch)
            }
        }
        stateHolder.dispatch(AppEvent.OnAppStarted)
    }

    private fun exceptionHandler(
        coroutineContext: CoroutineContext,
        throwable: Throwable,
    ) {
        // TODO proccess
    }
}
