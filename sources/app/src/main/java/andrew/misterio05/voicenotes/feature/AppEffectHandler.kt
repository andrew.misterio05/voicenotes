package andrew.misterio05.voicenotes.feature

import andrew.misterio05.domain_main.MainEffect
import andrew.misterio05.domain_notes.NotesEffect
import andrew.misterio05.tea.Effect
import andrew.misterio05.tea.EffectHandler
import andrew.misterio05.tea.Event

class AppEffectHandler(
    private val mainEffectHandler: EffectHandler<MainEffect>,
    private val notesEffectHandler: EffectHandler<NotesEffect>,
) : EffectHandler<Effect> {
    override suspend fun execute(
        effect: Effect,
        dispatch: (Event) -> Unit,
    ) {
        when (effect) {
            is MainEffect -> mainEffectHandler.execute(effect, dispatch)
            is NotesEffect -> notesEffectHandler.execute(effect, dispatch)
        }
    }
}
