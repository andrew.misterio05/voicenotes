package andrew.misterio05.voicenotes.di

import andrew.misterio05.voicenotes.feature.AppEffectHandler
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        AppModule::class,
        NotesModule::class,
    ],
)
@Singleton
interface AppComponent {
    val effectHandler: AppEffectHandler

    companion object {
        fun create(): AppComponent = DaggerAppComponent.builder().build()
    }
}
