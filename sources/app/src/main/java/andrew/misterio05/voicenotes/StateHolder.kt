package andrew.misterio05.voicenotes

import andrew.misterio05.tea.Effect
import andrew.misterio05.tea.Event
import andrew.misterio05.voicenotes.feature.AppReducer
import andrew.misterio05.voicenotes.feature.AppState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class StateHolder(state: AppState) {
    private val _state = MutableStateFlow(state)
    val state: StateFlow<AppState> = _state.asStateFlow()

    private val _effectFlow = MutableSharedFlow<Effect>()
    val effectFlow: Flow<Effect> = _effectFlow.asSharedFlow()

    private val scope: CoroutineScope = CoroutineScope(SupervisorJob())
    private val mutex = Mutex()

    fun dispatch(event: Event) {
        scope.launch {
            mutex.withLock(this@StateHolder) {
                val result = AppReducer(state.value, event)
                _state.value = result.state
                result.effects.forEach { effect -> _effectFlow.emit(effect) }
            }
        }
    }
}
