package andrew.misterio05.voicenotes.feature

import andrew.misterio05.domain_main.MainState
import andrew.misterio05.tea.State

data class AppState(
    val ui: MainState? = null,
) : State
