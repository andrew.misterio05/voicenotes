package andrew.misterio05.voicenotes.feature

import andrew.misterio05.tea.Event

interface AppEvent : Event {
    data object OnAppStarted : AppEvent
}
