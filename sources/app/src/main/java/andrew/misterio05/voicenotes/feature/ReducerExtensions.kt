package andrew.misterio05.voicenotes.feature

import andrew.misterio05.domain_main.MainState
import andrew.misterio05.tea.ReducerResult
import andrew.misterio05.tea.State
import andrew.misterio05.tea.just

internal fun AppState.updateMain(mainReducerResult: ReducerResult<MainState>) =
    ReducerResult(
        state = copy(ui = mainReducerResult.state),
        effects = mainReducerResult.effects,
    )

internal inline fun <reified T : State> MainState.updateScreen(reduce: T.() -> ReducerResult<T>): ReducerResult<MainState> {
    val subState by lazy { subStateStack.firstOrNull { it is T } }
    val bottom = bottomPart
    return when {
        bottom is T -> {
            bottom.reduce().let {
                ReducerResult(
                    state = copy(bottomPart = it.state),
                    effects = it.effects,
                )
            }
        }

        subState != null -> {
            (subState as T).reduce().let {
                ReducerResult(
                    state =
                        copy(
                            subStateStack =
                                subStateStack.set(
                                    index = subStateStack.indexOf(subState),
                                    element = it.state,
                                ),
                        ),
                    effects = it.effects,
                )
            }
        }

        else -> this.just()
    }
}
