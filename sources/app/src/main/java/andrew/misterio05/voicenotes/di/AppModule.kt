package andrew.misterio05.voicenotes.di

import andrew.misterio05.domain_main.MainEffectHandler
import andrew.misterio05.domain_notes.NotesEffectHandler
import andrew.misterio05.domain_notes.NotesRepository
import andrew.misterio05.voicenotes.feature.AppEffectHandler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun appEffectHandler(
        mainEffectHandler: MainEffectHandler,
        notesEffectHandler: NotesEffectHandler,
    ) = AppEffectHandler(
        mainEffectHandler = mainEffectHandler,
        notesEffectHandler = notesEffectHandler,
    )

    @Provides
    fun mainEffectHandler() = MainEffectHandler()

    @Provides
    fun notesEffectHandler(notesRepository: NotesRepository) =
        NotesEffectHandler(
            notesRepository = notesRepository,
        )
}
