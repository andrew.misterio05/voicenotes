package andrew.misterio05.voicenotes.feature

import andrew.misterio05.domain_main.MainEvent
import andrew.misterio05.domain_main.MainReducer
import andrew.misterio05.domain_main.MainState
import andrew.misterio05.domain_notes.NotesEffect
import andrew.misterio05.domain_notes.NotesEvent
import andrew.misterio05.domain_notes.NotesReducer
import andrew.misterio05.domain_notes.NotesState
import andrew.misterio05.domain_record.RecordEvent
import andrew.misterio05.domain_record.RecordReducer
import andrew.misterio05.domain_record.RecordState
import andrew.misterio05.tea.Event
import andrew.misterio05.tea.Reducer
import andrew.misterio05.tea.just
import andrew.misterio05.tea.with
import andrew.misterio05.voicenotes.R
import kotlinx.collections.immutable.persistentListOf

val AppReducer =
    Reducer<AppState, Event> { state, event ->
        when {
            state.ui != null ->
                when (event) {
                    is MainEvent -> state.updateMain(MainReducer(state.ui, event))

                    is NotesEvent -> {
                        state.updateMain(
                            state.ui.updateScreen<NotesState> {
                                NotesReducer(
                                    this,
                                    event,
                                )
                            },
                        )
                    }

                    is RecordEvent -> {
                        state.updateMain(
                            state.ui.updateScreen<RecordState> {
                                RecordReducer(
                                    this,
                                    event,
                                )
                            },
                        )
                    }

                    else -> {
                        state.just()
                    }
                }

            else ->
                when (event) {
                    is AppEvent.OnAppStarted ->
                        state.copy(
                            ui =
                                MainState(
                                    titleRes = R.string.app_name,
                                    subStateStack = persistentListOf(NotesState.init()),
                                    bottomPart = RecordState.new(),
                                ),
                        )
                            .with(NotesEffect.LoadList)

                    else -> state.just()
                }
        }
    }
