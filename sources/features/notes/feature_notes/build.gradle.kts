plugins {
    id("android.library.compose")
}

android {
    namespace = "andrew.misterio05.feature_notes"
}

dependencies {
    implementation(project(":domain_notes"))
    implementation(project(":uikit"))
    implementation(project(":tea"))
    implementation(libs.bundles.view)
}
