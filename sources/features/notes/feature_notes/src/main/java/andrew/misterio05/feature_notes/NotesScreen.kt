package andrew.misterio05.feature_notes

import andrew.misterio05.domain_notes.NotesEvent
import andrew.misterio05.domain_notes.NotesState
import andrew.misterio05.tea.Event
import andrew.misterio05.uikit.audioitem.AudioItemView
import android.text.format.DateFormat
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NotesScreen(
    modifier: Modifier,
    state: NotesState,
    dispatch: (Event) -> Unit,
) = Box(
    modifier = modifier,
) {
    LazyColumn(
        modifier =
            Modifier
                .fillMaxSize()
                .padding(horizontal = 16.dp),
    ) {
        items(state.notes) {
            Spacer(modifier = Modifier.size(8.dp))
            Card(
                elevation = CardDefaults.elevatedCardElevation(),
                onClick = { dispatch(NotesEvent.OnItemClicked(it.voiceId)) },
            ) {
                AudioItemView(
                    modifier =
                        Modifier
                            .padding(vertical = 4.dp, horizontal = 4.dp)
                            .padding(start = 4.dp),
                    title = it.title,
                    subtitle = it.time.dateToText(),
                    isPlaying = state.playingItem == it.voiceId,
                )
            }
        }
    }
}

@Composable
private fun Long.dateToText(): String {
    return DateFormat.format("yyyy-MM-dd HH:mm:ss", this).toString()
}
