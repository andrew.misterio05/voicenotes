package andrew.misterio05.feature_notes.data

import andrew.misterio05.domain_notes.NotesRepository
import andrew.misterio05.domain_notes.NotesState
import andrew.misterio05.domain_notes.VoiceId
import java.util.UUID

object NotesRepositoryMocks : NotesRepository {
    override suspend fun getAllNotes(): List<NotesState.Item> {
        return listOf(
            NotesState.Item(
                title = "Voice #1",
                time = System.currentTimeMillis(),
                voiceId = VoiceId(UUID.randomUUID().toString()),
            ),
            NotesState.Item(
                title = "Voice #2",
                time = System.currentTimeMillis(),
                voiceId = VoiceId(UUID.randomUUID().toString()),
            ),
        )
    }
}
