package andrew.misterio05.domain_notes

@JvmInline
value class VoiceId(val value: String)
