package andrew.misterio05.domain_notes

interface NotesRepository {
    suspend fun getAllNotes(): List<NotesState.Item>
}
