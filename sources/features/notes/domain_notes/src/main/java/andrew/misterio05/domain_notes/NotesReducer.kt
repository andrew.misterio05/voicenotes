package andrew.misterio05.domain_notes

import andrew.misterio05.tea.Reducer
import andrew.misterio05.tea.just
import kotlinx.collections.immutable.toPersistentList

val NotesReducer =
    Reducer<NotesState, NotesEvent> { state, event ->
        when (event) {
            is NotesEvent.OnNewList -> {
                state.copy(notes = event.value.toPersistentList()).just()
            }

            is NotesEvent.OnItemClicked -> {
                // TODO Send effect of playing previous item in state.playingItem
                //  Then send effect of stop playing next item in event.id
                state.copy(
                    playingItem =
                        when (state.playingItem) {
                            event.id -> null
                            else -> event.id
                        },
                )
                    .just()
            }
        }
    }
