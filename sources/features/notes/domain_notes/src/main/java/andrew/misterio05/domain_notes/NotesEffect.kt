package andrew.misterio05.domain_notes

import andrew.misterio05.tea.Effect

sealed interface NotesEffect : Effect {
    data object LoadList : NotesEffect
}
