package andrew.misterio05.domain_notes

import andrew.misterio05.tea.Event

sealed interface NotesEvent : Event {
    data class OnNewList(val value: List<NotesState.Item>) : NotesEvent

    data class OnItemClicked(val id: VoiceId) : NotesEvent
}
