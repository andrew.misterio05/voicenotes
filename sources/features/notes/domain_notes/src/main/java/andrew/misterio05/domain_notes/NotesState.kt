package andrew.misterio05.domain_notes

import andrew.misterio05.tea.State
import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.persistentListOf

data class NotesState internal constructor(
    val notes: PersistentList<Item>,
    val playingItem: VoiceId? = null,
) : State {
    data class Item(
        val voiceId: VoiceId,
        val title: String,
        val time: Long,
    )

    companion object {
        fun init() =
            NotesState(
                notes = persistentListOf(),
            )
    }
}
