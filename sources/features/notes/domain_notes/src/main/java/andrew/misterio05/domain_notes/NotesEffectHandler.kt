package andrew.misterio05.domain_notes

import andrew.misterio05.tea.EffectHandler
import andrew.misterio05.tea.Event

class NotesEffectHandler(
    private val notesRepository: NotesRepository,
) : EffectHandler<NotesEffect> {
    override suspend fun execute(
        effect: NotesEffect,
        dispatch: (Event) -> Unit,
    ) = when (effect) {
        is NotesEffect.LoadList -> dispatch(NotesEvent.OnNewList(notesRepository.getAllNotes()))
    }
}
