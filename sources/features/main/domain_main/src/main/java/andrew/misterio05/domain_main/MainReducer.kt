package andrew.misterio05.domain_main

import andrew.misterio05.tea.Reducer
import andrew.misterio05.tea.just

val MainReducer =
    Reducer<MainState, MainEvent> { state, event ->
        state.just()
    }
