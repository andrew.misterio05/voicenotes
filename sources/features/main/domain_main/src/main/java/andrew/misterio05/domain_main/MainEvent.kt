package andrew.misterio05.domain_main

import andrew.misterio05.tea.Event

sealed interface MainEvent : Event
