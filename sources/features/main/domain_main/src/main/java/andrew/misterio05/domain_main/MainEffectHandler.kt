package andrew.misterio05.domain_main

import andrew.misterio05.tea.EffectHandler
import andrew.misterio05.tea.Event

class MainEffectHandler : EffectHandler<MainEffect> {
    override suspend fun execute(
        effect: MainEffect,
        dispatch: (Event) -> Unit,
    ) {
    }
}
