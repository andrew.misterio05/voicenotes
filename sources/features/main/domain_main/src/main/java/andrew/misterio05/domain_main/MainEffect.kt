package andrew.misterio05.domain_main

import andrew.misterio05.tea.Effect

sealed interface MainEffect : Effect
