package andrew.misterio05.domain_main

import andrew.misterio05.tea.State
import kotlinx.collections.immutable.PersistentList

data class MainState(
    val titleRes: Int,
    val bottomPart: State?,
    val subStateStack: PersistentList<State>,
) : State {
    val currentSubState get() = subStateStack.lastOrNull()
}
