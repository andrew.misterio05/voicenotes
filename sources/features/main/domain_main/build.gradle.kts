plugins {
    id("kotlin.library")
}

dependencies {
    implementation(project(":tea"))
    implementation(libs.kotlinx.collections.immutable)
}
