package andrew.misterio05.feature_main

import andrew.misterio05.domain_main.MainState
import andrew.misterio05.tea.Event
import andrew.misterio05.tea.State
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp

@Composable
fun MainScreen(
    modifier: Modifier,
    state: MainState,
    screen: @Composable (Modifier, State) -> Unit,
    bottomPanel: @Composable (Modifier, State) -> Unit,
    dispatch: (Event) -> Unit,
) = Column(
    modifier = modifier,
) {
    Text(
        modifier = Modifier.padding(horizontal = 16.dp),
        text = LocalContext.current.getString(state.titleRes),
        style = MaterialTheme.typography.headlineLarge,
    )
    state.currentSubState?.let { screen(Modifier.fillMaxSize().weight(0.85f), it) }
    state.bottomPart?.let { bottomPanel(Modifier.fillMaxWidth().weight(0.15f), it) }
}
