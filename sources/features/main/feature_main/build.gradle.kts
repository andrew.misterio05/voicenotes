plugins {
    id("android.library.compose")
}

android {
    namespace = "andrew.misterio05.feature_main"
}

dependencies {
    implementation(project(":domain_main"))
    implementation(project(":tea"))
    implementation(libs.bundles.view)
}
