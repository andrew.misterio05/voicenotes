package andrew.misterio05.feature_record

import andrew.misterio05.domain_record.RecordEvent
import andrew.misterio05.domain_record.RecordState
import andrew.misterio05.tea.Event
import andrew.misterio05.uikit.icons.Icons
import andrew.misterio05.uikit.icons.rememberMic
import andrew.misterio05.uikit.icons.rememberSave
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun RecordScreen(
    modifier: Modifier,
    state: RecordState,
    dispatch: (Event) -> Unit,
) = Card(
    modifier = modifier.height(IntrinsicSize.Min),
    shape =
        MaterialTheme.shapes.extraLarge.copy(
            bottomEnd = CornerSize(0),
            bottomStart = CornerSize(0),
        ),
) {
    Row(
        modifier = Modifier.padding(16.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        when (state.stage) {
            is RecordState.Stage.Recording -> {
                // TODO Add timer with red circle
                SecondaryButton(
                    modifier = Modifier.weight(1f),
                    onClick = { dispatch(RecordEvent.OnRecordCancelRequest) },
                ) {
                    Icon(
                        modifier =
                            Modifier
                                .fillMaxSize(),
                        imageVector = androidx.compose.material.icons.Icons.Default.Close,
                        contentDescription = "Record button",
                    )
                }

                SecondaryButton(
                    modifier = Modifier.weight(1f),
                    onClick = { dispatch(RecordEvent.OnRecordSaveRequest) },
                ) {
                    Icon(
                        modifier = Modifier.fillMaxSize(),
                        imageVector = Icons.rememberSave(),
                        contentDescription = "Record button",
                    )
                }
            }

            is RecordState.Stage.Idle -> {
                Surface(
                    onClick = { dispatch(RecordEvent.OnRecordStart) },
                    modifier =
                        Modifier
                            .fillMaxSize()
                            .aspectRatio(1f, true),
                    color = MaterialTheme.colorScheme.primaryContainer,
                    contentColor = contentColorFor(MaterialTheme.colorScheme.primaryContainer),
                    shadowElevation = 8.dp,
                    shape = CircleShape,
                ) {
                    Icon(
                        modifier = Modifier.fillMaxSize(),
                        imageVector = Icons.rememberMic(),
                        contentDescription = "Record button",
                    )
                }
            }

            is RecordState.Stage.InputtingName -> {
                // TODO Make new voice name field
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = "",
                    onValueChange = {},
                )
            }
        }
    }
}

@Composable
private fun SecondaryButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    content: @Composable BoxScope.() -> Unit,
) = Box(
    modifier =
        modifier
            .aspectRatio(1f, true)
            .border(
                width = 2.dp,
                color = MaterialTheme.colorScheme.surface,
                shape = CircleShape,
            )
            .clickable(onClick = onClick)
            .padding(8.dp),
    content = content,
)
