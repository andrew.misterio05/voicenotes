plugins {
    id("android.library.compose")
}

android {
    namespace = "andrew.misterio05.feature_record"
}

dependencies {
    implementation(project(":domain_record"))
    implementation(project(":uikit"))
    implementation(project(":tea"))
    implementation(libs.bundles.view)
}
