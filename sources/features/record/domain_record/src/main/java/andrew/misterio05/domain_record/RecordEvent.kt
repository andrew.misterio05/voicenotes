package andrew.misterio05.domain_record

import andrew.misterio05.tea.Event

sealed interface RecordEvent : Event {
    data object OnRecordStart : RecordEvent

    data object OnRecordCancelRequest : RecordEvent

    data object OnRecordSaveRequest : RecordEvent

    data class OnNameInputted(val name: String) : RecordEvent
}
