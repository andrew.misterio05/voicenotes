package andrew.misterio05.domain_record

import andrew.misterio05.tea.Effect
import java.util.UUID

sealed interface RecordEffect : Effect {
    data object StartRecord : RecordEffect

    data object CancelRecord : RecordEffect

    data class SaveRecord(val id: UUID) : RecordEffect

    data class NameRecord(val id: UUID, val name: String) : RecordEffect
}
