package andrew.misterio05.domain_record

import andrew.misterio05.tea.Reducer
import andrew.misterio05.tea.just
import andrew.misterio05.tea.with
import java.util.UUID

val RecordReducer =
    Reducer<RecordState, RecordEvent> { state, event ->
        when {
            event is RecordEvent.OnRecordStart && state.stage is RecordState.Stage.Idle -> {
                state.copy(stage = RecordState.Stage.Recording)
                    .with(RecordEffect.StartRecord)
            }

            event is RecordEvent.OnRecordSaveRequest && state.stage is RecordState.Stage.Recording -> {
                val id = UUID.randomUUID()
                state.copy(stage = RecordState.Stage.InputtingName(id))
                    .with(RecordEffect.SaveRecord(id))
            }

            event is RecordEvent.OnNameInputted && state.stage is RecordState.Stage.InputtingName -> {
                state.with(RecordEffect.NameRecord(state.stage.id, event.name))
            }

            event is RecordEvent.OnRecordCancelRequest && state.stage is RecordState.Stage.Recording -> {
                state.copy(stage = RecordState.Stage.Idle).with(RecordEffect.CancelRecord)
            }

            else -> state.just()
        }
    }
