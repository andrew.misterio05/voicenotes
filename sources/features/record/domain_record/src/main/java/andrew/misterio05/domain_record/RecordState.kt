package andrew.misterio05.domain_record

import andrew.misterio05.tea.State
import java.util.UUID

data class RecordState(
    val stage: Stage,
) : State {
    sealed interface Stage {
        data object Recording : Stage

        data class InputtingName(val id: UUID) : Stage

        data object Idle : Stage
    }

    companion object {
        fun new() = RecordState(stage = Stage.Idle)
    }
}
