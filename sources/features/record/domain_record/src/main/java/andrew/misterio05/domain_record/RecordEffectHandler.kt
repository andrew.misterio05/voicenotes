package andrew.misterio05.domain_record

import andrew.misterio05.tea.EffectHandler
import andrew.misterio05.tea.Event

class RecordEffectHandler : EffectHandler<RecordEffect> {
    override suspend fun execute(
        effect: RecordEffect,
        dispatch: (Event) -> Unit,
    ) {
    }
}
