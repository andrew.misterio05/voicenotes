plugins {
    id("android.library.compose")
}

android {
    namespace = "andrew.misterio05.feature_main"
}

dependencies {
    implementation(libs.bundles.view)
}
