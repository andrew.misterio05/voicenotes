package andrew.misterio05.uikit.audioitem

import andrew.misterio05.uikit.icons.Icons
import andrew.misterio05.uikit.icons.rememberPauseCircle
import andrew.misterio05.uikit.icons.rememberPlayCircle
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun AudioItemView(
    modifier: Modifier = Modifier,
    title: String,
    subtitle: String,
    isPlaying: Boolean,
) = Row(
    modifier = modifier.height(IntrinsicSize.Min),
    verticalAlignment = Alignment.CenterVertically,
) {
    Column(
        modifier = Modifier.weight(1f),
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.titleMedium,
            maxLines = 1,
        )
        Text(
            text = subtitle,
            style = MaterialTheme.typography.labelMedium,
            maxLines = 1,
        )
    }

    Icon(
        modifier =
            Modifier
                .fillMaxHeight()
                .aspectRatio(1f),
        imageVector =
            when {
                isPlaying -> Icons.rememberPauseCircle()
                else -> Icons.rememberPlayCircle()
            },
        contentDescription =
            when {
                isPlaying -> "pause icon"
                else -> "play icon"
            },
    )
}
