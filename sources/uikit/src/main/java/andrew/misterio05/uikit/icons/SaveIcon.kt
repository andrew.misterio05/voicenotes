package andrew.misterio05.uikit.icons

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathFillType
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.path
import androidx.compose.ui.unit.dp

@Composable
fun Icons.rememberSave(): ImageVector {
    return remember {
        ImageVector.Builder(
            name = "save",
            defaultWidth = 40.0.dp,
            defaultHeight = 40.0.dp,
            viewportWidth = 40.0f,
            viewportHeight = 40.0f,
        ).apply {
            path(
                fill = SolidColor(Color.Black),
                fillAlpha = 1f,
                stroke = null,
                strokeAlpha = 1f,
                strokeLineWidth = 1.0f,
                strokeLineCap = StrokeCap.Butt,
                strokeLineJoin = StrokeJoin.Miter,
                strokeLineMiter = 1f,
                pathFillType = PathFillType.NonZero,
            ) {
                moveTo(7.875f, 34.75f)
                quadToRelative(-1.042f, 0f, -1.833f, -0.792f)
                quadToRelative(-0.792f, -0.791f, -0.792f, -1.833f)
                verticalLineTo(7.875f)
                quadToRelative(0f, -1.042f, 0.792f, -1.833f)
                quadToRelative(0.791f, -0.792f, 1.833f, -0.792f)
                horizontalLineToRelative(19.292f)
                quadToRelative(0.541f, 0f, 1.041f, 0.208f)
                quadToRelative(0.5f, 0.209f, 0.875f, 0.584f)
                lineToRelative(4.875f, 4.875f)
                quadToRelative(0.375f, 0.375f, 0.584f, 0.875f)
                quadToRelative(0.208f, 0.5f, 0.208f, 1.041f)
                verticalLineToRelative(19.292f)
                quadToRelative(0f, 1.042f, -0.792f, 1.833f)
                quadToRelative(-0.791f, 0.792f, -1.833f, 0.792f)
                close()
                moveToRelative(24.25f, -21.875f)
                lineToRelative(-5f, -5f)
                horizontalLineTo(7.875f)
                verticalLineToRelative(24.25f)
                horizontalLineToRelative(24.25f)
                close()
                moveTo(20f, 29.667f)
                quadToRelative(1.833f, 0f, 3.104f, -1.271f)
                quadToRelative(1.271f, -1.271f, 1.271f, -3.104f)
                quadToRelative(0f, -1.834f, -1.271f, -3.125f)
                quadToRelative(-1.271f, -1.292f, -3.104f, -1.292f)
                quadToRelative(-1.833f, 0f, -3.104f, 1.292f)
                quadToRelative(-1.271f, 1.291f, -1.271f, 3.125f)
                quadToRelative(0f, 1.791f, 1.271f, 3.083f)
                quadToRelative(1.271f, 1.292f, 3.104f, 1.292f)
                close()
                moveToRelative(-8.75f, -13.709f)
                horizontalLineToRelative(12.083f)
                quadToRelative(0.584f, 0f, 0.938f, -0.354f)
                reflectiveQuadToRelative(0.354f, -0.937f)
                verticalLineTo(11.25f)
                quadToRelative(0f, -0.583f, -0.354f, -0.937f)
                quadToRelative(-0.354f, -0.355f, -0.938f, -0.355f)
                horizontalLineTo(11.25f)
                quadToRelative(-0.583f, 0f, -0.938f, 0.355f)
                quadToRelative(-0.354f, 0.354f, -0.354f, 0.937f)
                verticalLineToRelative(3.417f)
                quadToRelative(0f, 0.583f, 0.354f, 0.937f)
                quadToRelative(0.355f, 0.354f, 0.938f, 0.354f)
                close()
                moveToRelative(-3.375f, -3.083f)
                verticalLineToRelative(19.25f)
                verticalLineToRelative(-24.25f)
                close()
            }
        }.build()
    }
}
