# Voice notes

A project showcasing my skills in creating an Android application.
It's still in progress, but there's already something to see.

Kotlin, Multimodules, TheElm Architecture, Coroutines, Flow, Dagger2, JetpackCompose, convention plugins, and even more coming soon.
